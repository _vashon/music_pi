"""unattended music service"""
import sys
from time import time, sleep
import urllib.request
from urllib.parse import urlparse
import shutil
import subprocess
import os
import pika

class ServiceConfiguration(object):
    INTERVAL = 'interval'
    SERVICE_BUS_URL = 'servicebusurl'
    SERVICE_PORT = 'serviceport'
    SERVICE_BUS_USER = 'servicebususer'
    SERVICE_BUS_PASS = 'servicebuspassword'
    SERVICE_BUS_VIRTUAL_HOST = 'servicebusvirtualbus'
    settings = {
        SERVICE_BUS_URL:'',
        SERVICE_PORT:0,
        SERVICE_BUS_VIRTUAL_HOST:'',
        SERVICE_BUS_USER:'',
        SERVICE_BUS_PASS:'',
        INTERVAL:10
        }
    def __setattr__(self, *_):
        pass

CONFIG = ServiceConfiguration()

def main():
    process_interval_seconds = CONFIG.settings[CONFIG.INTERVAL]

    send_log("service starting")

    while True:
        start = time()
        try:
            check_for_message(process_message)
            sleep(start + process_interval_seconds - time())
        except KeyboardInterrupt:
            print("main; interrupted by user")
            send_log("interrupted by user")
            break
        except InterruptedError:
            print("main; interrupted")
            send_log("interrupted")
            break
        except TypeError as error:
            print("main; TypeError: {0}".format(error))
        except ValueError as error:
            print("main; ValueError: {0}".format(error))
        except:
            error = sys.exc_info()[0]
            print("main; except: {0}".format(error))
        finally:
            print(".")

    send_log("service stopping")

def handle_connection_error(connection_unused, error_message):
    print('conn err: ' + error_message.strerror)

def handle_connection_open(method_frame):
    print('conn open: ' + method_frame)

def handle_declare_queue():
    pass

def configure_connection():
    credentials = pika.PlainCredentials(
        username=CONFIG.settings[CONFIG.SERVICE_BUS_USER],
        password=CONFIG.settings[CONFIG.SERVICE_BUS_PASS]
        )
    connection_parameters = pika.ConnectionParameters(
        host=CONFIG.settings[CONFIG.SERVICE_BUS_URL],
        port=CONFIG.settings[CONFIG.SERVICE_PORT],
        virtual_host=CONFIG.settings[CONFIG.SERVICE_BUS_VIRTUAL_HOST],
        credentials=credentials
        )

    return connection_parameters

def check_for_message(process):
    try:
        connection_parameters = configure_connection()

        # connection = pika.BaseConnection(
        #    parameters=connection_parameters,
        #    on_open_callback=handle_connection_open,
        #    on_open_error_callback=handle_connection_error
        #    )
        # channel = connection.channel(on_open_callback=handle_connection_open)
        # channel.queue_declare(callback=handle_declare_queue,queue='ongaku-gg')

        connection = pika.BlockingConnection(connection_parameters)
        channel = connection.channel()
        channel.queue_declare(queue='ongaku-gg', durable=True)

        message = channel.basic_get(queue='ongaku-gg')
        if process(message):
            channel.basic_ack(delivery_tag=message[0].delivery_tag)

        # NON-BLOCKING: CALLBACK
        # channel.basic_consume(callback,
        #                       queue='ongakumsg',
        #                       no_ack=True)
        # channel.start_consuming()

        # BLOCKING; RETURNS TUPLE
        # channel.consume()

        connection.close()        
    except TypeError as error:
        print("check_for_message; TypeError: {err}".format(err=error))
    except pika.exceptions.ConnectionClosed as error:
        print("check_for_message; ConnectionClosed: {err}".format(err=error))
    except pika.exceptions.ChannelClosed as error:
        print("check_for_message; ChannelClosed: {err}".format(err=error))
    except:
        error = sys.exc_info()[0]
        print("check_for_message; exception: {err}".format(err=error))

def process_message(message):
    success = False

    commands = dict();
    commands['start_music'] = start_music
    commands['stop_music'] = stop_music
    commands['start_access'] = start_access
    commands['stop_access'] = stop_music
    commands['send_status'] =  send_status
    commands['get_file'] = get_file
    commands['get_source'] = get_source

    try:
        
        # reboot
        # play playlist
        # remove/delete song (hide?, backup?)

        if message[2] != None:
            print("received message: " + message[2].decode("utf-8"))
            command = message[2].split()
            commands[command[0]](command[1])
            success = True
    except TypeError as error:
        print("process_message; TypeError: {0}".format(error))
    except ValueError as error:
        print("process_message; ValueError: {0}".format(error))
    except:
        error = sys.exc_info()[0]
        print("process_message; except: {0}".format(error))

    return success

def start_music():
    cmd = ['systemctl', 'start music.service']
    result = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    err = result.stderr.decode('utf-8')
    if err.length > 0:
        send_log(err)
    # result.stdout.decode('utf-8')

def stop_music():
    cmd = ['systemctl', 'stop music.service']
    result = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    err = result.stderr.decode('utf-8')
    if err.length > 0:
        send_log(err)

def start_access():
    cmd = ['systemctl', 'start access.service']
    result = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    err = result.stderr.decode('utf-8')
    if err.length > 0:
        send_log(err)

def stop_access():
    cmd = ['systemctl', 'start access.service']
    result = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    err = result.stderr.decode('utf-8')
    if err.length > 0:
        send_log(err)

def send_status():
    send_log('TODO Status')

def get_file(url):
    parsed_url = urlparse(url)
    file_name = os.path.basename(parsed_url.path)
    file_path = '/music/' + file_name 
    with urllib.request.urlopen(url) as response, open(file_path, 'wb') as out_file:
        shutil.copyfileobj(response, out_file)

def get_source():
    cmd = ['force_update', '']
    result = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    err = result.stderr.decode('utf-8')
    if err.length > 0:
        send_log(err)  

def send_log(message):
    try:
        connection_parameters = configure_connection()

        # connection = pika.BaseConnection(
        #    parameters=connection_parameters,
        #    on_open_callback=handle_connection_open,
        #    on_open_error_callback=handle_connection_error
        #    )
        # channel = connection.channel(on_open_callback=handle_connection_open)
        # channel.queue_declare(callback=handle_declare_queue,queue='ongaku-gg')

        connection = pika.BlockingConnection(connection_parameters)
        channel = connection.channel()

        channel.exchange_declare(exchange='ongakulog',
                                 type='fanout',
                                 durable=True)
        if channel.basic_publish(exchange='ongakulog', routing_key='', body=message):
            print("send_log; Sent: {msg}".format(msg=message))
        # p = channel.publish(exchange='ongakulog', routing_key='', body=message)
        connection.close()
    except TypeError as error:
        print("sendLog; TypeError: {err}".format(err=error))
    except ValueError as error:
        print("sendLog; ValueError: {err}".format(err=error))
    except pika.exceptions.ConnectionClosed as error:
        print("sendLog; ConnectionClosed: {err}".format(err=error))
    except pika.exceptions.ChannelClosed as error:
        print("sendLog; ChannnelClosed: {err}".format(err=error))
        # print("sendLog; ChannnelClosed: {error}".format_map(locals()))
    except:
        error = sys.exc_info()[0]
        print("send_log; except: {err}".format(err=error))

def report_error(error):
    print(error)
    send_log(error)

if __name__ == '__main__':
    sys.exit(main())
